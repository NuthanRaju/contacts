//
//  Person+CoreDataProperties.swift
//  ContactApplication
//
//  Created by Nuthan raju on 07/08/19.
//  Copyright © 2019 Google. All rights reserved.
//
//

import Foundation
import CoreData


extension Person {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Person> {
        return NSFetchRequest<Person>(entityName: "Person")
    }

    @NSManaged public var name: String?
    @NSManaged public var phNumber: String?
    @NSManaged public var alternatePhNumber: String?
    @NSManaged public var emailId: String?

}
