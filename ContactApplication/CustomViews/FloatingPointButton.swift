//
//  FloatingPointButton.swift
//  ContactApplication
//
//  Created by Nuthan raju on 07/08/19.
//  Copyright © 2019 Google. All rights reserved.
//
import UIKit
import Foundation
class FloatingPointButton: UIButton {
    override func draw(_ rect: CGRect) {
        layer.backgroundColor = UIColor.red.cgColor
        layer.cornerRadius = frame.height/2
        layer.shadowOpacity = 0.25
        layer.shadowOffset = CGSize(width: 0, height: 10)
        layer.shadowRadius = 5
    }
}
