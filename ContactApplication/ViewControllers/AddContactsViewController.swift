//
//  AddContactsViewController.swift
//  ContactApplication
//
//  Created by Nuthan raju on 06/08/19.
//  Copyright © 2019 Google. All rights reserved.
//

import UIKit
protocol DataSender {
    func sendData(person: Person)
    func update()
}

class AddContactsViewController: UIViewController {
    
    var contactsData = [String: [Person]]()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phNoTextField: UITextField!
    @IBOutlet weak var alternatePhNoTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    var editPerson: Person?
    var isEditable: Bool = false
    var titleText = "Add Contacts"
    var delegate: DataSender? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = titleText
        if let person = editPerson {
            nameTextField.text = person.name
            phNoTextField.text = person.phNumber
            alternatePhNoTextField.text = person.alternatePhNumber
            emailTextField.text = person.emailId
        }
        
        
    }
    
    @IBAction func cancelButton(_ sender: FloatingPointButton) {
        self.dismiss(animated: true)
    }
    
    @IBAction func saveButton(_ sender: FloatingPointButton) {
        if !isEditable {
            if nameTextField.text == "" {
                showAlert(title: "Name Field is Empty", message: "Enter your Name")
                return
            }
           
            if phNoTextField.text == "" {
                showAlert(title: "Phone number is empty", message: "Enter your phone number")
                return
                
            }
            guard let numberText =  phNoTextField.text, numberText.count <= 10 else {
                showAlert(title: "Invalid Phone Number", message: "Phone Number must be 10 digits long")
                return
            }
            
            
            do {
                let person = Person(context: PersistenceServices.context)
                person.name = nameTextField.text
                person.phNumber = phNoTextField.text
                for tuple in contactsData.enumerated() {
                    let key = tuple.element.key
                    let persons = contactsData[key]
                    for person in persons! {
                        if person.phNumber == phNoTextField.text {
                        showAlert(title: "Phone Number must be Unique", message: "Please Enter a Valid PhoneNumber")
                           return
                    }
                }
                }
                person.alternatePhNumber = alternatePhNoTextField.text
                person.emailId = emailTextField.text
                try PersistenceServices.context.save()
                delegate?.sendData(person: person)
            } catch {
                print(error)
            }
            self.dismiss(animated: true)
        }
        else {
            nameTextField.becomeFirstResponder()
            editPerson?.name = nameTextField.text ?? ""
            editPerson?.phNumber = phNoTextField.text ?? ""
            editPerson?.alternatePhNumber = alternatePhNoTextField.text ?? ""
            editPerson?.emailId = emailTextField.text ?? ""
            do {
                try PersistenceServices.context.save()
            } catch {
                print(error)
            }
            delegate?.update()
            self.dismiss(animated: true)
            
        }
        
    }
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default) { (action) in
            print("Ok pressed")
        }
        alert.addAction(ok)
        self.present(alert, animated: true)
    }
    
    @IBAction func doneAction(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
    
    @IBAction func action(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
    
}
