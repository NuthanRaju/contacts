//
//  ContactsViewController.swift
//  ContactApplication
//
//  Created by Nuthan raju on 06/08/19.
//  Copyright © 2019 Google. All rights reserved.
//

import UIKit
import CoreData
class ContactsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var personsDict: [String: [Person]] = ["A":[],"B":[],"C":[],"D":[],"E":[],"F":[],"G":[],"H":[],"I":[],"J":[],"K":[],"L":[],"M":[],"N":[],"O":[],"P":[],"Q":[],"R":[],"S":[],"T":[],"U":[],"V":[],"W":[],"X":[],"Y":[],"Z":[]]
    
    var personsSectionTitles = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = "Contacts"
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let persons = try! PersistenceServices.context.fetch(fetchRequest)
            for person in persons {
                if let  startLetter = person.name?.first {
                    let  letterString  = "\(startLetter)"
                    self.personsDict[letterString]?.append(person)
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    @IBAction func addButtonTapped(_ sender: FloatingPointButton) {
        let VC = storyboard?.instantiateViewController(withIdentifier: String(describing: AddContactsViewController.self)) as! AddContactsViewController
         VC.contactsData = self.personsDict
          VC.delegate = self
        
        self.present(VC, animated: true)
        
        
    }
}

extension ContactsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return personsSectionTitles.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let personKey = personsSectionTitles[section]
        guard let personValues = personsDict[personKey] else { return 0}
        return personValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? ContactsTableViewCell {
            let personKey = personsSectionTitles[indexPath.section]
            if let personValues = personsDict[personKey] {
                let person = personValues[indexPath.row]
                cell.nameLabel.text = person.name
                cell.phNoLabel.text = person.phNumber
            }
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return personsSectionTitles[section]
    }
    
}
extension ContactsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = storyboard?.instantiateViewController(withIdentifier: String(describing: ContactsDetailViewController.self)) as! ContactsDetailViewController
        let personKey = personsSectionTitles[indexPath.section]
        if let personValues = personsDict[personKey] {
            let person = personValues[indexPath.row]
            detailVC.selectedperson = person
            }
        self.present(detailVC, animated: true)
    }
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = deletedAction(at: indexPath)
        let config = UISwipeActionsConfiguration(actions: [delete])
        config.performsFirstActionWithFullSwipe = false
        return config
    }
    func deletedAction(at indexPath: IndexPath)-> UIContextualAction {
        let action = UIContextualAction(style: .destructive, title: "Delete") { (action, view, completion) in
            
            for tuple in self.personsDict.enumerated() {
                let key = tuple.element.key
                let persons = tuple.element.value
                for (index, _) in persons.enumerated() {
                    let person = self.personsDict[key]!.remove(at: index)
                    PersistenceServices.context.delete(person)
                    do {
                        try PersistenceServices.context.save()
                    } catch {
                        print(error)
                    }
                    
                }
            }
            completion(true)
            self.tableView.reloadData()
            
        }
        action.image = UIImage(named: "Delete")
        return action
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let edit = editAction(at: indexPath)
        let config = UISwipeActionsConfiguration(actions: [edit])
        config.performsFirstActionWithFullSwipe = false
        return config
    }
    func editAction(at indexPath: IndexPath)-> UIContextualAction {
        let action = UIContextualAction(style: .normal, title: "Edit") { (action, view, completion) in
            let editVC = self.storyboard?.instantiateViewController(withIdentifier: String(describing: AddContactsViewController.self)) as! AddContactsViewController
            editVC.titleText = "Edit Contact"
            let personKey = self.personsSectionTitles[indexPath.section]
            if let personValues = self.personsDict[personKey] {
                let person = personValues[indexPath.row]
                editVC.isEditable = true
                editVC.editPerson = person
                editVC.delegate = self
                self.tableView.reloadData()
            }
            
            self.present(editVC, animated: true)
            completion(true)
        }
        action.image = UIImage(named: "Edit")
        action.backgroundColor = .orange
        return action
    }
}
extension ContactsViewController: DataSender {
    func sendData(person: Person) {
        if let  startLetter = person.name!.first {
            let  letterString  = "\(startLetter)"
            self.personsDict[letterString]?.append(person)
            self.tableView.reloadData()
        }
        
    }
    func update() {
        for tuple in personsDict.enumerated() {
            let key = tuple.element.key
            let persons = tuple.element.value
            for (index, person) in persons.enumerated() {
                let firstLetter = "\(person.name!.first!)"
                if firstLetter != key {
                    personsDict[key]!.remove(at: index)
                    personsDict[firstLetter]?.append(person)
                }
            }
        }
        self.tableView.reloadData()
    }
    
    
}
