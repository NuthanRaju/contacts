//
//  ContactsDetailViewController.swift
//  ContactApplication
//
//  Created by Nuthan raju on 06/08/19.
//  Copyright © 2019 Google. All rights reserved.
//

import UIKit

class ContactsDetailViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phNoLabel: UILabel!
    @IBOutlet weak var alternatePhNumber: UILabel!
    @IBOutlet weak var emailIdLabel: UILabel!
    var selectedperson: Person?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let person = selectedperson {
            nameLabel.text = person.name
            phNoLabel.text = person.phNumber
            alternatePhNumber.text = person.alternatePhNumber
            emailIdLabel.text = person.emailId
        }
    }
    
    @IBAction func doneButton(_ sender: Any) {
        self.dismiss(animated: true)
    }
}
